﻿using System.ComponentModel.DataAnnotations;

namespace MovieProject.Models.DTOs.Movie
{
    public class MovieReadDTO
    {
        [Key] public int MovieID { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        [MaxLength(4)]
        public int Year { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int Franchise{ get; set; }
             
    }
}