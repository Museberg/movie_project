﻿using System.ComponentModel.DataAnnotations;

namespace MovieProject.Models.DTOs.Franchise
{
    public class FranchiseReadDTO
    {
        [Key] public int FranchiseID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
