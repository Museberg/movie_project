﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject.Models.Domain
{
    public class Franchise
    {
        [Key]
        public int FranchiseID { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(1024)]
        public string Description { get; set; }
        
        // Relationships
        public List<Movie> Movies { get; set; }
    }
}