﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject.Models.Domain
{
    public class Movie
    {
        [Key]
        public int MovieID { get; set; }
        [MaxLength(50)]
        public string Title { get; set; }
        [MaxLength(128)]
        public string Genre { get; set; }
        [MaxLength(4)] 
        public int Year { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(256)]
        public string Picture { get; set; }
        [MaxLength(256)]
        public string Trailer { get; set; }
        
        // Relationships
        public List<Character> Characters { get; set; }
        public Franchise Franchise { get; set; }
    }
}