﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject.Models.Domain
{
    public class Character
    {
        [Key]
        public int CharacterID { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(20)]
        public string Gender { get; set; }
        [MaxLength(256)]
        public string Picture { get; set; }
        
        // Relationships
        public List<Movie> Movies { get; set; }
    }
}