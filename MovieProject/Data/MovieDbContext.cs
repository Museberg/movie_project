﻿using Microsoft.EntityFrameworkCore;
using MovieProject.Models.Domain;
using System.Configuration;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace MovieProject.Data
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var movies = new List<Object>
            {
                new
                {
                    MovieID = 1,
                    Title = "The Terminator",
                    Genre = "Action, Sci-Fi",
                    Director = "James Cameron",
                    Picture = "https://www.imdb.com/title/tt0088247/mediaviewer/rm774208512/?ref_=tt_ov_i",
                    Trailer = "https://www.youtube.com/watch?v=k64P4l2Wmeg",
                    Year = 1984,
                    FranchiseID = 1
                },
                new
                {
                    MovieID = 2,
                    Title = "Terminator 2",
                    Genre = "Action, Sci-Fi",
                    Director = "James Cameron",
                    Picture = "https://www.imdb.com/title/tt0103064/mediaviewer/rm4264138753/?ref_=tt_ov_i",
                    Trailer = "https://www.youtube.com/watch?v=lwSysg9o7wE",
                    Year = 1991,
                    FranchiseID = 1
                },
                new
                {
                    MovieID = 3,
                    Title = "Iron Man",
                    Genre = "Action, Adventure, Sci-Fi",
                    Director = "Jon Favreau",
                    Picture = "https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/?ref_=tt_ov_i",
                    Trailer = "https://www.youtube.com/watch?v=8ugaeA-nMTc",
                    Year = 2008,
                    FranchiseID = 2
                },
                new
                {
                    MovieID = 4,
                    Title = "Iron Man 2",
                    Genre = "Action, Adventure, Sci-Fi",
                    Director = "Jon Favreau",
                    Picture = "https://www.imdb.com/title/tt1228705/mediaviewer/rm1059163136/?ref_=tt_ov_i",
                    Trailer = "https://www.youtube.com/watch?v=wKtcmiifycU",
                    Year = 2010,
                    FranchiseID = 2
                }
            };

            var franchises = new List<Franchise>()
            {
                new Franchise()
                {
                    FranchiseID = 1,
                    Name = "Terminator Franchise",
                    Description = "All the Terminator movies from James and others",
                },
                new Franchise()
                {
                    FranchiseID = 2,
                    Name = "Marvel Cinematic Universe",
                    Description = "All of the MCU movies, not the TV-shows (except maybe Loki and Wandavision)",
                }
            };

            var characters = new List<Character>()
            {
                new Character()
                {
                    CharacterID = 1,
                    FullName = "Arnold Schwarzenegger",
                    Alias = "The Terminator",
                    Gender = "Cyborg",
                    Picture = "https://www.imdb.com/name/nm0000216/?ref_=tt_cl_i_1"
                },
                new Character()
                {
                    CharacterID = 2,
                    FullName = "Linda Hamilton",
                    Alias = "Sarah Connor",
                    Gender = "Female",
                    Picture = "https://www.imdb.com/name/nm0000157/?ref_=tt_cl_i_2"
                },
                new Character()
                {
                    CharacterID = 3,
                    FullName = "Robert Downey Jr.",
                    Alias = "Tony Stark",
                    Gender = "Male",
                    Picture = "https://www.imdb.com/name/nm0000375/?ref_=tt_cl_i_1"
                },
                new Character()
                {
                    CharacterID = 4,
                    FullName = "Paul Bettany",
                    Alias = "JARVIS (voice)",
                    Gender = "AI",
                    Picture = "https://www.imdb.com/name/nm0079273/?ref_=tt_cl_i_11"
                }
            };

            builder.Entity<Movie>()
                .HasOne(m => m.Franchise)
                .WithMany(c => c.Movies)
                .HasForeignKey("FranchiseID");
            
            builder.Entity<Franchise>().HasData(franchises);
            builder.Entity<Character>().HasData(characters);
            builder.Entity<Movie>().HasData(movies);
            

            builder.Entity<Movie>()
                .HasMany<Character>(m => m.Characters)
                .WithMany(c => c.Movies)
                .UsingEntity(j => j.ToTable("CharacterMovie")
                    .HasData(
                        new { MoviesMovieID = 1, CharactersCharacterID = 1 }, // Adding Schwarzenegger to The Terminator
                        new { MoviesMovieID = 1, CharactersCharacterID = 2 }, // Adding Hamilton to The Terminator
                        new { MoviesMovieID = 2, CharactersCharacterID = 1}, // Adding Schwarzenegger to Terminator 2
                        new { MoviesMovieID = 2, CharactersCharacterID = 2}, // Adding Hamilton to Terminator 2
                        new { MoviesMovieID = 3, CharactersCharacterID = 3}, // Adding Downey Jr. to Iron Man
                        new { MoviesMovieID = 3, CharactersCharacterID = 4}, // Adding Bettany to Iron Man
                        new { MoviesMovieID = 4, CharactersCharacterID = 3}, // Adding Donwey Jr. to Iron Man 2
                        new { MoviesMovieID = 4, CharactersCharacterID = 4} // Adding Bettany to Iron Man 2
                        )
                );
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
    }
}