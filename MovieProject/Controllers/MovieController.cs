﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieProject.Models.Domain;
using MovieProject.Models.DTOs.Movie;
using AutoMapper;
using MovieProject.Data;

namespace MovieProject.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        /// <summary>
        /// The constructor for the MovieController
        /// </summary>
        /// <param name="context">The database context</param>
        /// <param name="mapper">The automapper to map from domain to DTO</param>
        public MovieController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies in the database
        /// </summary>
        /// <returns>Returns a list of all movies in the database</returns>
        [HttpGet]
        public async Task<ActionResult<List<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies.ToListAsync());
        }

        /// <summary>
        /// Gets a movie with a given ID
        /// </summary>
        /// <param name="id">The ID of the movie to get</param>
        /// <returns>404 if movie with ID was not found. Otherwise the movie is returned</returns>
        [HttpGet("{id}")] 
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Edits the movie with the given ID
        /// </summary>
        /// <param name="id">The ID of movie to edit</param>
        /// <param name="movie">The new values of the movie given as an object</param>
        /// <returns>Status 400 if incorrect ID given. 404 if movie with ID not found. 204 if edit was a success</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.MovieID)
            {
                return BadRequest();
            }
            Movie domainMovie = _mapper.Map<Movie>(movie);
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        
        /// <summary>
        /// Creates a new movie
        /// </summary>
        /// <param name="movie">The new movie to save to the database</param>
        /// <returns>Status 201</returns>
        [HttpPost]
        public async Task<ActionResult> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.MovieID }, movie);
        }

        /// <summary>
        /// Deletes the movie with the given ID
        /// </summary>
        /// <param name="id">The ID of the movie to delete</param>
        /// <returns>404 if movie with ID could not be found. 204 if movie with ID was found and deleted</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
                  
        /// <summary>
        /// Checks if movie with given ID exists
        /// </summary>
        /// <param name="id">The ID of the movie to look for</param>
        /// <returns>True if movie with ID exists, false if not</returns>
        private bool MovieExists(int id)
            {
                return _context.Movies.Any(x => x.MovieID == id);
            }                      
    }
}