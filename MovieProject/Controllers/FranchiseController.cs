﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieProject.Models.Domain;
using MovieProject.Models.DTOs.Franchise;
using AutoMapper;
using MovieProject.Data;

namespace MovieProject.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for FranchiseController
        /// </summary>
        /// <param name="context">The database context</param>
        /// <param name="mapper">The automapper to map objects from domain to DTO</param>
        public FranchiseController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns>Returns a list of all franchises</returns>
        [HttpGet]
        public async Task<ActionResult<List<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises.ToListAsync());
        }

        /// <summary>
        /// Gets the franchise with the given ID
        /// </summary>
        /// <param name="id">The ID of the franchise to find</param>
        /// <returns>The franchise with the given ID</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Edits the franchise
        /// </summary>
        /// <param name="id">The ID of the franchise to edit</param>
        /// <param name="franchise">The new values of the franchise given as an object</param>
        /// <returns>Status 400 if incorrect ID given. 404 if franchise with ID not found. 204 if edit was a success</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            if (id != franchise.FranchiseID)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        
        /// <summary>
        /// Creates a new franchise
        /// </summary>
        /// <param name="franchise">The franchise to save to the database</param>
        /// <returns>Status 201</returns>
        [HttpPost]
        public async Task<ActionResult> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.FranchiseID }, franchise);
        }

        /// <summary>
        /// Deletes the franchise with the given ID
        /// </summary>
        /// <param name="id">The ID of the franchise to delete</param>
        /// <returns>404 if franchise with ID not found. 204 if franchise with ID was found and deleted</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Checks if franchise with ID exists
        /// </summary>
        /// <param name="id">The ID to look for</param>
        /// <returns>True if franchise with ID exists, false if not</returns>
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(x => x.FranchiseID == id);
        }
    }
}