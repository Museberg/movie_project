﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieProject.Models.Domain;
using MovieProject.Models.DTOs.Character;
using AutoMapper;
using MovieProject.Data;

namespace MovieProject.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor for CharacterController
        /// </summary>
        /// <param name="context">The database context</param>
        /// <param name="mapper">The automapper for mapping from domain to DTO</param>
        public CharacterController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all characters in the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters.ToListAsync());
        }

        /// <summary>
        /// Returns the character with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The character with the given ID or status 404 if character not found</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Edits the character with the given ID
        /// </summary>
        /// <param name="id">The ID of the character to edit</param>
        /// <param name="character">The properties to edit - passed as a character object</param>
        /// <returns>Status 400 if incorrect ID given. 404 if user with ID not found. 204 if edit was a success</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutCharacter(int id, CharacterEditDTO character)
        {
            if (id != character.CharacterID)
            {
                return BadRequest();
            }
            Character domainCharacter = _mapper.Map<Character>(character);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        /// <summary>
        /// Saves the provided character to the database
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Status 201</returns>
        [HttpPost]
        public async Task<ActionResult> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.CharacterID }, character);
        }

        /// <summary>
        /// Deletes character with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>404 if character with ID could not be found. 204 if character was found and deleted</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Provide ID to check if character exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if character could be found, false if not</returns>
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(x => x.CharacterID == id);
        }
    }
}