﻿using AutoMapper;
using MovieProject.Models.Domain;
using MovieProject.Models.DTOs.Franchise;

namespace MovieProject.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();
     
            CreateMap<Franchise, FranchiseCreateDTO>();

            CreateMap<Franchise, FranchiseEditDTO>();
        }
    }
}