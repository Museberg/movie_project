﻿using MovieProject.Models.DTOs.Character;
using AutoMapper;
using MovieProject.Models.Domain;

namespace MovieProject.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>();

            CreateMap<Character, CharacterCreateDTO>();
            
            CreateMap<Character, CharacterEditDTO>();
        }
    }
}