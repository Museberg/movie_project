﻿using MovieProject.Models.DTOs.Movie;
using AutoMapper;
using MovieProject.Models.Domain;

namespace MovieProject.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>();

            CreateMap<Movie, MovieCreateDTO>();

            CreateMap<Movie, MovieReadDTO>();
        }
    }
}